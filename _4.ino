int glosnik = 9;
int reset = 10;
int dioda =11;
int szukana;
bool traf;
void setup() {
  Serial.begin(9600);
  pinMode(glosnik,OUTPUT);
  pinMode(5,INPUT);
  pinMode(reset,INPUT_PULLUP);
  pinMode(dioda,OUTPUT);
  // put your setup code here, to run once:
  randomSeed(analogRead(9));
  szukana=random(823)+100;
}

void loop() {
  if (digitalRead(reset)==LOW){
    szukana=random(823)+100;
    noTone(glosnik);
    traf=0;
    digitalWrite(dioda,LOW);
  }
  int strzal=analogRead(A1);
  if (abs(strzal-szukana)<20){
    digitalWrite(dioda,HIGH);
    traf=1;
    tone(glosnik,650);
  }
  if (!(traf))
    {
    Serial.println(analogRead(A1));
    tone(glosnik,650);
    delay(10);
    noTone(glosnik);
    delay(abs(strzal-szukana));
    }
  // put your main code here, to run repeatedly:

}
